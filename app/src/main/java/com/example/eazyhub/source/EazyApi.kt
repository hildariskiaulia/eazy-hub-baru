package com.example.eazyhub.source

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object EazyApi {
    private const val BASE_URL = "https://ihsmart-api-dev.vsan-apps.playcourt.id/"
    private const val AUTH_TOKEN = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJubyI6IjQ5YWVjZjJlLTQ0NjEtNDg1OC04NzA0LTNmYTk0ZDU2N2VlMyIsImFwaVZlcnNpb24iOjMsInNpZ25hdHVyZSI6ImlocyIsImlhdCI6MTY5MzIxNDAwMSwiZXhwIjoxNjkzODE4ODAxLCJhdWQiOiI5N2IzMzE5My00M2ZmLTRlNTgtOTEyNC1iM2E5YjlmNzJjMzQiLCJpc3MiOiJkZXZ0ZWxrb20ifQ.LTJb08CJbaCaHs-4VpyePDFdIPf4NmydygWaZL7W-fsLRRBdfA5b8plXUFPA2IkUx9xkLxAWhh5PXL94ku5IzZALUX6n2zyNPWTrLfjyl4Hnufz6iaWEKBVHxGUsLz1FtWsuIPDvYEpzt2yYrj8nJw0sC76TsAQIjZJCOmNYg6WameMHx_cotUcE1d2uieZbTHB2o53bG4dPK--vCxHsaeSROZUmO7mhc0O_bQ6yjwR9dwHVodU43ZJx83i3Qe0jWsCf3u4bhwyQfYmV_YJTTJFLCD5muqNMdhcCOa2ER0mOMNYZz4SDPtjoihx2Qz98qpOAn4UKAaO_ga46imwUH3n4zHgx9gvP1Y-RsI9UKnm1OLwAY9KOsTNr8zo4caSYx8JogZpFCBarru9iT7WJePOkEGiL67-dgvw_Jb6LB2UlBB5_shJAYqdOrZJN31EE4PULDIWgsLsHesMXPwC9vB22PciIDtDuxPcjpiHx-vAXxj0sWne8U8kqDQ3nuTgkXBeJprP1J5S-gRHO2ZXVGzGh296XVN8Az0X9Ixo0iXLuPuDuOuHeGAfVvo8idMy7GPAO5CjS2q3cboij5SVXQLmBm_X5rhmOgfyaxXP_MobsY6-uLB64aePpmG1FC9DAfhCEaydZwjcTlJTeunPt81JninuQGPhAi3tkzqSkJPQ"

    private val retrofit: Retrofit by lazy {
        val interceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor { chain ->
                val request = chain.request().newBuilder()
                    .addHeader("Authorization", "Bearer $AUTH_TOKEN")
                    .build()
                chain.proceed(request)
            }
            .build()

        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(Json {
                isLenient = true
                ignoreUnknownKeys = true
            }.asConverterFactory("application/json".toMediaType()))
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val retrofitService: EazyApiService by lazy {
        retrofit.create(EazyApiService::class.java)
    }
}