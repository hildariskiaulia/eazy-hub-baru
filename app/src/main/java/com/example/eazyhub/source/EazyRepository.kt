package com.example.eazyhub.source

import com.example.eazyhub.model.OrderData

class EazyRespository {
    suspend fun getUserData(userId: String, token: String): OrderData {
        val data = EazyApi.retrofitService.getUserData(userId, token)
        return data
    }

    companion object {
        @Volatile
        private var instance: EazyRespository? = null

        fun getInstance(): EazyRespository =
            instance ?: synchronized(this) {
                EazyRespository().apply {
                    instance = this
                }
            }
    }
}

