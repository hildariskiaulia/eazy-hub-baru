package com.example.eazyhub.source

import com.example.eazyhub.model.OrderData
import retrofit2.http.GET
import retrofit2.http.Path

interface EazyApiService {
    @GET("users/v4/profile/{userId}")
    suspend fun getUserData(
        @Path("userId") userId: String,
        token: String,
    ): OrderData
}
