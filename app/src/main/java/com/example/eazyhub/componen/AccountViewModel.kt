package com.example.eazyhub.componen

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eazyhub.model.OrderData
import com.example.eazyhub.model.UserDataResponse
import com.example.eazyhub.source.EazyRespository
import com.example.eazyhubnew.util.UiState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch


class AccountViewModel(
    private val repository: EazyRespository
) : ViewModel() {
    private val _uiState: MutableStateFlow<UiState<OrderData>> =
        MutableStateFlow(UiState.Loading)
    val uiState: StateFlow<UiState<OrderData>>
        get() = _uiState

    private val _state = MutableStateFlow(
        UserDataResponse(
            userId = "",
            fullname = "",
            email = "",
            profile = ""
        )
    )
    val state = _state.asStateFlow()

    fun getUserData(userId: String, token:String) {
        viewModelScope.launch {
            val userData = repository.getUserData(userId, "Bearer $token")
            Log.d("Hilda",userData.data.toString())
            _state.value = (userData.data.get(0))
        }
    }
}