package com.example.eazyhub.componen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import com.example.eazyhub.R
import com.example.eazyhub.ui.theme.EazyHubTheme
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


@Composable
fun AccountScreen(modifier: Modifier = Modifier, qrValue: String) {
    val name : MutableState<String?> = remember { mutableStateOf("") }
    val userId : MutableState<String?> = remember { mutableStateOf("") }
    LaunchedEffect(Unit) {
        getUserIdFromFirebase(qrValue, callback={userId.value=it})
        getNameFromFirebase(qrValue, callback = {name.value=it})
    }

    Column(modifier = modifier) {
        Row() {
            Column(
                verticalArrangement = Arrangement.spacedBy(20.dp, Alignment.Top),
                horizontalAlignment = Alignment.Start,
            ) {
                Text(
                    text = stringResource(id = R.string.account),
                    color = Color.White,
                    fontWeight = FontWeight.Bold,
                    fontSize = 20.sp,
                )
                LazyRow(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .width(width = 550.dp)
                        .clip(shape = RoundedCornerShape(10.dp))
                        .background(color = Color(0x80000000), shape = RoundedCornerShape(size = 10.dp))
                        .padding(start = 20.dp, top = 15.dp, end = 20.dp, bottom = 15.dp)
                ) {
                    item {
                        Row(verticalAlignment = Alignment.CenterVertically) {
                            Box(
                                modifier = Modifier
                                    .size(size = 60.dp)
                                    .clip(shape = CircleShape)
                                    .background(color = Color(0xffd9d9d9))
                            )
                            Spacer(modifier = Modifier.width(width = 20.dp))
                            Column() {
                                Text(
                                    text = name.value?:"",
                                    color = Color.White,
                                    lineHeight = 1.5.em,
                                    style = TextStyle(
                                        fontSize = 16.sp,
                                        fontWeight = FontWeight.Bold
                                    )
                                )
                                Spacer(modifier = Modifier.height(height = 2.dp))
                                Text(
                                    text = userId.value?:"",
                                    color = Color.White,
                                    lineHeight = 1.5.em,
                                    style = TextStyle(
                                        fontSize = 14.sp
                                    )
                                )
                            }
                        }
                        Spacer(modifier = Modifier.width(width = 20.dp))
                    }
                    item {
                        Text(
                            text = "Sign Out",
                            color = Color(0xff2970ff),
                            lineHeight = 1.5.em,
                            style = TextStyle(
                                fontSize = 15.sp,
                                fontWeight = FontWeight.Bold
                            ),
                            modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp)
                        )
                    }
                }
            }
        }
        Column(
            modifier = Modifier
                .width(width = 744.dp)
        ) {
            Text(
                text = "Paket Saya",
                color = Color.White,
                fontWeight = FontWeight.Bold,
                fontSize = 20.sp,
                modifier = modifier.padding(top = 20.dp, end = 20.dp, bottom = 20.dp)

            )
            LazyRow(
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .width(width = 550.dp)
                    .clip(shape = RoundedCornerShape(1.dp))
                    .background(color = Color(0x80000000), shape = RoundedCornerShape(size = 10.dp))
                    .padding(start = 20.dp, top = 13.dp, end = 20.dp, bottom = 13.dp)
            ) {
                item {
                    Text(
                        text = "Antares",
                        color = Color.White,
                        lineHeight = 1.5.em,
                        style = TextStyle(
                            fontSize = 15.sp,
                            fontWeight = FontWeight.Bold
                        ),
                        modifier = Modifier.fillMaxWidth()
                    )
                    Spacer(modifier = Modifier.width(width = 20.dp))
                }
                item {
                    Text(
                        text = "Active until 3 March 2023",
                        color = Color.White,
                        textAlign = TextAlign.End,
                        lineHeight = 1.5.em,
                        style = TextStyle(
                            fontSize = 15.sp
                        ),
                        modifier = Modifier.fillMaxWidth()
                    )
                }
            }
            Spacer(modifier = Modifier.height(height = 15.dp))
            LazyRow(
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .width(width = 550.dp)
                    .clip(shape = RoundedCornerShape(1.dp))
                    .background(color = Color(0x80000000), shape = RoundedCornerShape(size = 10.dp))
                    .padding(start = 20.dp, top = 13.dp, end = 20.dp, bottom = 13.dp)
            ) {
                item {
                    Text(
                        text = "Eazy",
                        color = Color.White,
                        lineHeight = 1.5.em,
                        style = TextStyle(
                            fontSize = 15.sp,
                            fontWeight = FontWeight.Bold
                        ),
                        modifier = Modifier.fillMaxWidth()
                    )
                    Spacer(modifier = Modifier.width(width = 3.dp))
                }
                item {
                    Text(
                        text = "Active until 4 May 2023",
                        color = Color.White,
                        textAlign = TextAlign.End,
                        lineHeight = 1.5.em,
                        style = TextStyle(
                            fontSize = 15.sp
                        ),
                        modifier = Modifier.fillMaxWidth()
                    )
                }
            }
            Spacer(modifier = Modifier.height(height = 15.dp))
            LazyRow(
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .width(width = 550.dp)
                    .clip(shape = RoundedCornerShape(1.dp))
                    .background(color = Color(0x80000000), shape = RoundedCornerShape(size = 10.dp))
                    .padding(start = 20.dp, top = 13.dp, end = 20.dp, bottom = 13.dp)
            ) {
                item {
                    Text(
                        text = "Sooltancam",
                        color = Color.White,
                        lineHeight = 1.5.em,
                        style = TextStyle(
                            fontSize = 15.sp,
                            fontWeight = FontWeight.Bold
                        ),
                        modifier = Modifier.fillMaxWidth()
                    )
                    Spacer(modifier = Modifier.width(width = 3.dp))
                }
                item {
                    Text(
                        text = "Active until 12 June 2023",
                        color = Color.White,
                        textAlign = TextAlign.End,
                        lineHeight = 1.5.em,
                        style = TextStyle(
                            fontSize = 15.sp
                        ),
                        modifier = Modifier.fillMaxWidth()
                    )
                }
            }
        }
    }
}
fun getNameFromFirebase(name: String, callback: (String?) -> Unit) {
    val database = FirebaseDatabase.getInstance()
    val myRef: DatabaseReference = database.reference.child("LINK_ACCOUNT").child("ANDROID_TV")

    myRef.addListenerForSingleValueEvent(object : ValueEventListener {
        override fun onDataChange(dataSnapshot: DataSnapshot) {
            val userNode = dataSnapshot.child(name)
            val nameValue = userNode.child("name").getValue(String::class.java)
            callback(nameValue)
        }

        override fun onCancelled(error: DatabaseError) {
            callback(null)
        }
    })
}
fun getUserIdFromFirebase(userId: String, callback: (String?) -> Unit) {
    val database = FirebaseDatabase.getInstance()
    val myRef: DatabaseReference = database.reference.child("LINK_ACCOUNT").child("ANDROID_TV")

    myRef.addListenerForSingleValueEvent(object : ValueEventListener {
        override fun onDataChange(dataSnapshot: DataSnapshot) {
            val userNode = dataSnapshot.child(userId)
            val userIdValue = userNode.child("userId").getValue(String::class.java)
            callback(userIdValue)
        }

        override fun onCancelled(error: DatabaseError) {
            callback(null)
        }
    })
}

@Preview(showBackground = true)
@Composable
fun AccountScreenPreview() {
    EazyHubTheme {
        AccountScreen(qrValue = "")
    }
}