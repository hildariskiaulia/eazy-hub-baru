package com.example.eazyhub.componen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.eazyhub.ui.theme.EazyHubTheme
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.Exclude
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.IgnoreExtraProperties
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.QRCodeWriter


@Composable
fun PopupLoginQR(modifier: Modifier, onClosePopup: () -> Unit, onLoginSuccess: () -> Unit, qrValue: String) {

    Column(
        modifier = Modifier
            .background(color = Color.Black)
    ) {
        Row(
            horizontalArrangement = Arrangement.End,
            modifier = Modifier.fillMaxWidth()
        ) {
            IconButton(
                onClick = onClosePopup,
                modifier = Modifier.padding(15.dp)
            ) {

            }
        }
        LazyRow(
            verticalAlignment = Alignment.Top,
            modifier = Modifier.padding(horizontal = 35.dp)
        ) {
            item {
                Column {
                }
                Box(
                    modifier = Modifier
                        .width(width = 4.dp)
                        .height(height = 180.dp)

                )
                Spacer(modifier = Modifier.width(width = 40.dp))
                var qrValue by remember { mutableStateOf("") }
                qrValue = getUserIDFromFirebase()

                val qrCodeBitmap = generateQRCodeBitmap(qrValue)

                if (qrCodeBitmap != null) {
                    Image(
                        bitmap = qrCodeBitmap.asImageBitmap(),
                        contentDescription = "QR Code",
                        modifier = Modifier
                            .size(size = 180.dp)
                            .clickable {
                                onClosePopup()
                                onLoginSuccess()
                            }
                    )
                }
            }
        }
    }
}

fun getUserIDFromFirebase(): String {
    val database = FirebaseDatabase.getInstance()
    val myRef: DatabaseReference = database.reference.child("LINK_ACCOUNT").child("ANDROID_TV")
    val newKey: String? = myRef.push().key
    val auth = FirebaseAuth.getInstance()

    if (newKey != null) {
        auth.signInAnonymously().addOnCompleteListener { signInTask ->
            if (signInTask.isSuccessful) {
                val user = auth.currentUser
                val newUser = User(author = user?.uid, name = "Hilda Rizki Aulia")
                val postValues = newUser.toMap()

                val childUpdates = hashMapOf<String, Any>(
                    newKey to postValues
                )
                myRef.updateChildren(childUpdates)
            }
        }
    }

    return newKey ?: ""
}


fun generateQRCodeBitmap(qrValue: String): android.graphics.Bitmap? {
    val hints: MutableMap<EncodeHintType, Any> = hashMapOf()
    hints[EncodeHintType.CHARACTER_SET] = "UTF-8"

    try {
        val writer = QRCodeWriter()
        val bitMatrix: BitMatrix = writer.encode(qrValue, BarcodeFormat.QR_CODE, 512, 512, hints)
        val width = bitMatrix.width
        val height = bitMatrix.height
        val pixels = IntArray(width * height)

        for (y in 0 until height) {
            val offset = y * width
            for (x in 0 until width) {
                pixels[offset + x] = if (bitMatrix[x, y]) android.graphics.Color.BLACK else android.graphics.Color.WHITE
            }
        }

        val bitmap = android.graphics.Bitmap.createBitmap(width, height, android.graphics.Bitmap.Config.RGB_565)
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
        return bitmap

    } catch (e: WriterException) {
        e.printStackTrace()
    }

    return null
}


@IgnoreExtraProperties
data class User(val author: String? = null, val name: String? = null) {
    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "author" to author,
            "name" to name
        )
    }
}

@Preview(showBackground = true)
@Composable
fun PopupLoginQRPreview() {
    EazyHubTheme {
        PopupLoginQR(modifier = Modifier, onClosePopup = {}, onLoginSuccess = {}, qrValue = "")
    }
}


