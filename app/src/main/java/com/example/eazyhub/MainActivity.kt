package com.example.eazyhub

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.eazyhub.componen.PopupLoginQR
import com.example.eazyhub.ui.theme.EazyHubTheme
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase

class MainActivity : ComponentActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            EazyHubTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    PopupLoginQR(modifier = Modifier, onClosePopup = {}, onLoginSuccess = {}, qrValue = "")
                }
            }
        }

        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        // Write a message to the database
        val database = Firebase.database
        val myRef = database.reference.child("LINK_ACCOUNT").child("COBA")

        // Read from the database
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                val value = dataSnapshot.getValue<Any>()
                Log.d("MainActivity", "Value is: $value")
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w("MainActivity", "Failed to read value.", error.toException())
            }
        })

        // Authenticate anonymously
        auth = Firebase.auth
        auth.signInAnonymously()
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(ContentValues.TAG, "signInAnonymously:success")
                    val user = auth.currentUser
                    Log.d("MainActivity", "user: ${user?.uid}")

                    val newKey = myRef.child("LINK_ACCOUNT").push().key
                    val newUser = User(author = user?.uid, name = "Hilda")
                    val postValues = newUser.toMap()

                    val childUpdates = hashMapOf<String, Any>(
                        "/LINK_ACCOUNT/COBA/COBA$newKey" to postValues
                    )

                    myRef.updateChildren(childUpdates)

                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(ContentValues.TAG, "signInAnonymously:failure", task.exception)
                    Toast.makeText(
                        baseContext,
                        "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()

                }
            }
    }



    @IgnoreExtraProperties
    data class User(val author: String? = null, val name: String? = null) {
        @Exclude
        fun toMap(): Map<String, Any?> {
            return mapOf(
                "author" to author,
                "name" to name
            )
        }
    }
}
