package com.example.eazyhub.di

import com.example.eazyhub.componen.AccountViewModel
import com.example.eazyhub.source.EazyRespository
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single { EazyRespository.getInstance()}
    viewModel { AccountViewModel(get()) }
}