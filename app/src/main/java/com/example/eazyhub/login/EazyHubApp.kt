package com.example.eazyhub.login

import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.eazyhub.componen.AccountScreen
import com.example.eazyhub.componen.PopupLoginQR
import com.example.eazyhub.ui.theme.EazyHubTheme


@Composable
fun EazyHubApp(modifier: Modifier = Modifier) {
    val navController = rememberNavController()
    var showPopup by remember { mutableStateOf(false) }
    var qrValue by remember { mutableStateOf("xxx") }


    NavHost(navController = navController, startDestination = "login") {
        composable("login") {
            LoginScreen(modifier = modifier, { showPopup = true }, onQrGenerate={qrValue=it})
        }
        composable("account") {
            AccountScreen(modifier = modifier, qrValue=qrValue)
        }

    }
    if (showPopup) {
        PopupLoginQR(
            modifier = Modifier.width(700.dp),
            onClosePopup = { showPopup = false }, onLoginSuccess = { navController.navigate("account")}, qrValue = qrValue
        )
    }
}




@Preview(showBackground = true)
@Composable
fun EazyHubAppPreview() {
    EazyHubTheme {
        EazyHubApp()
    }
}
