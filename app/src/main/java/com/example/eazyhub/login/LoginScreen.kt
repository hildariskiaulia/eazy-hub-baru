package com.example.eazyhub.login

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.eazyhub.R
import com.example.eazyhub.componen.getUserIDFromFirebase
import com.example.eazyhub.ui.theme.EazyHubTheme

@Composable
fun LoginScreen(modifier: Modifier = Modifier, onShowQr:()->Unit, onQrGenerate: (String) -> Unit){
    LaunchedEffect(Unit) {
        val qrValue = getUserIDFromFirebase()
        onQrGenerate(qrValue)
    }

    Column(modifier = modifier) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Row{
            }
            Column(modifier = modifier) {
                Text(
                    text = stringResource(id = R.string.account),
                    color = Color.White,
                    fontWeight = FontWeight.Bold,
                    fontSize = 20.sp,
                    modifier = modifier
                        .padding(bottom = 20.dp)
                )
                Button(
                    onClick = {},
                    shape = RoundedCornerShape(20),
                    colors = ButtonDefaults.buttonColors(containerColor = Color.White),
                    modifier = modifier
                        .width(550.dp)
                ) {
                    Row(
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Icon(
                            painter = painterResource(id = R.drawable.google_logo),
                            contentDescription = null,
                            tint = Color.Unspecified,
                            modifier = Modifier
                                .size(30.dp)
                                .padding(end = 8.dp)
                        )
                        Text(
                            text = "Sign in using Google Account",
                            color = Color.Black,
                            modifier = Modifier.padding(start = 8.dp)
                        )
                        Spacer(modifier = Modifier.weight(1f))
                    }
                }
                Spacer(modifier = modifier.padding(vertical = 8.dp))
                Button(
                    onClick = {
                        onShowQr()
                    },
                    shape = RoundedCornerShape(20),
                    colors = ButtonDefaults.buttonColors(
                        containerColor = Color(0x80000000),
                        contentColor = Color.White
                    ),
                    modifier = modifier
                        .width(550.dp)

                ) {
                    Row(
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Icon(
                            painter = painterResource(id = R.drawable.qr),
                            contentDescription = null,
                            tint = Color.Unspecified,
                            modifier = Modifier
                                .size(30.dp)
                                .padding(end = 8.dp)
                        )
                        Text(
                            text = "Sign in Using QR Smart Home App",
                            color = Color.White,
                            modifier = Modifier.padding(start = 8.dp)
                        )
                        Spacer(modifier = Modifier.weight(1f))
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun LoginScreenPreview() {
    EazyHubTheme {
        LoginScreen(onShowQr = {}, onQrGenerate = {})
    }
}