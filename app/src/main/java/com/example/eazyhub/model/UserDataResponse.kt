package com.example.eazyhub.model

import kotlinx.serialization.Serializable

@Serializable
class UserDataResponse (
    val userId: String,
    val fullname: String,
    val email: String,
    val profile: String
)
