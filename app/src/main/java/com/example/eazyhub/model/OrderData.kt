package com.example.eazyhub.model

import com.example.eazyhub.model.UserDataResponse
import kotlinx.serialization.Serializable

@Serializable
data class OrderData(
    val data: List <UserDataResponse>
)